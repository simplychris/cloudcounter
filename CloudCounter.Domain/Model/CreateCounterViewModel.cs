﻿namespace CloudCounter.Domain.Model
{
    public class CreateCounterViewModel
    {
        public Counter NewCounter;
        public Account OwnerAccount;
    }
}