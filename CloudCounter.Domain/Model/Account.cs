﻿using System;
using System.Collections.Generic;

namespace CloudCounter.Domain.Model
{
    public class Account
    {
        public Int32 Id;
        public String Name;
        public String Password;

        public IList<Counter> Counters { get; set; }

        public Account ()
        {
            Counters = new List<Counter>();
        }
    }
}