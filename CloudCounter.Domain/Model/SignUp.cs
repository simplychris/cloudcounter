﻿using System.ComponentModel.DataAnnotations;

namespace CloudCounter.Domain.Model
{
    public class SignUp
    {
        public SignUp ()
        {
            
        }
        [Required]
        public string UserName { set; get; }
        public string Password1 { set; get; }
        public string Password2  { set; get; }
    }
}