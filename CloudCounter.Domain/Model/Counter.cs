﻿using System;

namespace CloudCounter.Domain.Model
{
    public class Counter
    {
        public Int32 Count { get; set; }
        public String Id { get; set; }
        public String CreatorApiCode { get; set; }
        public String Description { get; set; }
        public Int32 OwnerAccountId { get; set; }
        public DateTime LastUpdated { get; set; }

        public Counter () : this (-1)
        {
            
        }

        public Counter (Int32 ownerId)
        {
            Id = Guid.NewGuid().ToString();
            Count = 0;
            OwnerAccountId = ownerId;
        }



        public Int32 Increment (Int32 amount)
        {
            Count = Count + amount;
            LastUpdated = DateTime.UtcNow;
            return Count;
        }

        public Int32 GetCount ()
        {
            return Count;
        }
    }
}
