﻿using System;

namespace CloudCounter.Domain.Model
{
    public class SimpleMessage
    {
        public String Message { get; set; } 
    }
}