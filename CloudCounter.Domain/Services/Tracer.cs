﻿using System;
using System.Net.Http;
using System.Web.Http.Tracing;
using CloudCounter.Domain.Interface;
using StructureMap;
using log4net;
namespace CloudCounter.Domain.Services
{
    public class Tracer : ITraceWriter
    {
        
        public void Trace(HttpRequestMessage request, string category, TraceLevel level, Action<TraceRecord> traceAction)
        {
            var record = new TraceRecord(request, category, level);

            traceAction(record);
        }


        public void WriteTrace(TraceRecord record)
        {
            var logger = ObjectFactory.GetInstance<ILogFactory>().GetLog<Tracer>();
            var message = string.Format("{0};{1};{2}", record.Operator, record.Operation, record.Message);
            logger.Debug(message);
        }
    }
}