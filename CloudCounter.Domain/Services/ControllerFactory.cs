﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using CloudCounter.Domain.IoC;

namespace CloudCounter.Domain.Services
{
    public class ControllerFactory : DefaultControllerFactory 
    {
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {

            IController controller;
            if (controllerType == null)
                return null;
            try
            {
                controller = (IController) IoCContainer.GetInstance(controllerType);
            }

            catch (Exception ex)
            {
                return base.GetControllerInstance(requestContext, controllerType);
            }
            
            if (controller==null)
            {
                controller =  base.GetControllerInstance(requestContext, controllerType);
            }

            return (controller);
        }
    }
}