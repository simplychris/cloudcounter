﻿using CloudCounter.Domain.Interface;
using log4net;
using log4net.Core;

namespace CloudCounter.Domain.Services
{
    public class LogFactory : ILogFactory 
    {
        public ILog GetLog<T>()
        {
            return LogManager.GetLogger(typeof(T));            
        }
    }
}