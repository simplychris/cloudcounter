﻿using CloudCounter.Domain.Interface;
using CloudCounter.Domain.Repository;
using CloudCounter.Domain.Services;
using StructureMap.Configuration.DSL;
using log4net;
using log4net.Core;

namespace CloudCounter.Domain.IoC
{
    public class CloudCounterRegistry : Registry
    {
        public CloudCounterRegistry ()
        {            
            Scan(scanner =>
                     {
                         scanner.AssembliesFromApplicationBaseDirectory();
                         scanner.LookForRegistries();
                         scanner.TheCallingAssembly();
                         scanner.WithDefaultConventions();
                     });
                      
                For<ICloudCounterRepository>().Singleton().Use<LinqMemoryCounterRepository>();                
                For(typeof(ILog)).Use(LogManager.GetLogger("TraceLog")    );
                For(typeof (ILogFactory)).Use(new LogFactory());
                For(typeof (ILogger<>)).Use(typeof (Log4NetLogger<>));            
        }         
        }
             
}