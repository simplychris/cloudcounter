﻿using System;
using CloudCounter.Domain.Interface;
using CloudCounter.Domain.Services;
using StructureMap;
using log4net;

namespace CloudCounter.Domain.IoC
{
    public static class IoCContainer
    {
        private static ILog _logger;
        public static IContainer Container
        {
            get; set; //get { return ObjectFactory.Container; }
        }

        static IoCContainer ()
        {
            Container = new Container();    
        }

         public static void Configure ()
         {             
             Container.Configure(config => config.Scan(scan =>
             {
                 scan.AssembliesFromApplicationBaseDirectory();
                 scan.LookForRegistries();
                 scan.TheCallingAssembly();
                 scan.WithDefaultConventions();
             }));
             Container.Configure(init => init.AddRegistry<CloudCounterRegistry>());
                          
             _logger = GetInstance<ILog>();
             _logger.DebugFormat("StructureMap has been Initialized");
             _logger.DebugFormat(ObjectFactory.WhatDoIHave());
         }

        public static T GetInstance <T> ()
        {
            if (_logger!=null)
                _logger.DebugFormat("Request For Type <T>: {0}", typeof(T));       
            return Container.GetInstance<T>();
        }

        public static object GetInstance (Type type)
        {
            if (_logger != null)            
                _logger.DebugFormat("Request For Type: {0}", type);       
            return Container.GetInstance(type);
        }
    }}