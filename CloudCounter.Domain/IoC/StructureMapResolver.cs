﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;
using StructureMap;

namespace CloudCounter.Domain.IoC
{
    public class StructureMapResolver : IDependencyResolver
    {
        private readonly IContainer _container;

        public void Dispose()
        {
            
        }

        public void Build ()
        {
            IoCContainer.Configure();
        }

        public StructureMapResolver  (IContainer container)
        {
            _container = container;
        }

        public object GetService(Type serviceType)
        {
            if (serviceType == null) return null;
            try
            {
                return serviceType.IsAbstract || serviceType.IsInterface
                         ? _container.TryGetInstance(serviceType)
                         : _container.GetInstance(serviceType);
            }
            catch
            {

                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _container.GetAllInstances(serviceType).Cast<object>();
        }

        public IDependencyScope BeginScope()
        {            
            return this;
        }
    }
}