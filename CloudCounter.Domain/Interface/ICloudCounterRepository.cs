﻿namespace CloudCounter.Domain.Interface
{
    public interface ICloudCounterRepository : IAccountRepository, ICounterRepository
    {
         
    }
}