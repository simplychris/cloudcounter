﻿using System.Collections.Generic;
using CloudCounter.Domain.Model;

namespace CloudCounter.Domain.Interface
{
    public interface ICounterRepository
    {
        void SaveCounter (Counter counter);
        Counter GetCounter (string counterId);
        IList<Counter> GetCounters(int accountId);
        void UpdateCounter (Counter counter);
        void DeleteCounter(string counterid);
    }    
}