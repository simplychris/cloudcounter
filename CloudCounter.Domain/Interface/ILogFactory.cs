﻿using log4net;

namespace CloudCounter.Domain.Interface
{
    public interface ILogFactory
    {
        ILog GetLog<T>();
    }
}