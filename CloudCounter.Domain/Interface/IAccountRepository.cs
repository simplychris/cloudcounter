﻿using System;
using CloudCounter.Domain.Model;

namespace CloudCounter.Domain.Interface
{
    public interface IAccountRepository
    {
        void SaveAccount(Account account);
        Account GetAccount(Int32 accountId);
        Account GetAccount(String username);
    }
}