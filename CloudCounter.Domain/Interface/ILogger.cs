﻿using log4net;

namespace CloudCounter.Domain.Interface
{
    public interface ILogger <T>
    {

        void Debug (string message);
        void DebugFormat(string format, params object[]parameters );
    }
}