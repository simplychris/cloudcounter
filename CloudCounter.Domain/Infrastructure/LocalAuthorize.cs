﻿using System.Web;
using System.Web.Mvc;
using CloudCounter.Domain.Model;

namespace CloudCounter.Domain.Infrastructure
{
    public class LocalAuthorize : AuthorizeAttribute 
    {
//        protected override bool AuthorizeCore(HttpContextBase httpContext)
//        {            
//            if (!httpContext.User.Identity.IsAuthenticated)
//                return false;
//            
//
//            return true;
//        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var controllerContext = new ControllerContext (filterContext.RequestContext, filterContext.Controller);
            var controller = (string) filterContext.RouteData.Values["controller"];
            var action = (string) filterContext.RouteData.Values["action"];

            var model = new SimpleMessage() {Message = "LocalAuthorize Says: You are not authorized to view this page."};

            filterContext.Result = new ViewResult {ViewName = "~/Views/Shared/SimpleMessage.cshtml", ViewData = new ViewDataDictionary<SimpleMessage>(model)};
        }
    }
}