﻿using System;
using System.Web;

namespace CloudCounter.Domain.Infrastructure
{
    public class ActionResult
    {
        public Int32 HttpStatusCode { get; set; }
        public Object Data { get; set; }

        public ActionResult ()
        {        
        }
    }
}