﻿namespace CloudCounter.Domain.Infrastructure
{
    public class SessionNames
    {
        public static readonly string UserSession = "UserSession";
    }
}