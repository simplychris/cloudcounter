﻿using CloudCounter.Domain.Interface;
using CloudCounter.Domain.IoC;
using CloudCounter.Domain.Model;

namespace CloudCounter.Domain.Utility
{
    public class SeedDataHelper
    {
        public ICloudCounterRepository _repository;
        
        public SeedDataHelper (ICloudCounterRepository repository)
        {
            //_repository = IoCContainer.GetInstance<ICloudCounterRepository>();
            _repository = repository;

        }

        public void SeedCounterData01 (int recordCount)
        {
            for (var i = 0; i < recordCount; i++)
            {
                _repository.SaveCounter(new Counter(902));
            }
        }

    }
}