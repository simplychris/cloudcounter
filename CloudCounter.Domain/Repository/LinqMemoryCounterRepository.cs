﻿using System;
using System.Collections.Generic;
using CloudCounter.Domain.Interface;
using CloudCounter.Domain.Model;
using System.Linq;
using System.Linq.Expressions;
namespace CloudCounter.Domain.Repository
{
    public class LinqMemoryCounterRepository : ICloudCounterRepository 
    {
        private IList<Counter> _counters { get; set; }
        private IList<Account> _accounts { get; set; } 
        
        public LinqMemoryCounterRepository ()
        {
            _counters = new List<Counter>();
            _accounts = new List<Account>();
        }

        
        public void SaveCounter (Counter counter)
        {
            if (_counters.SingleOrDefault(x => x.Id == counter.Id) != null)
                throw new CounterRepositoryException(string.Format("Id {0} exisits in storage already",
                                                                   counter.Id.ToString()));
            _counters.Add(counter);
        }

        public Counter GetCounter (string counterId)
        {            
            var counter = _counters.SingleOrDefault(x => x.Id == counterId);
            if (counter == null)            
                throw new CounterRepositoryException(string.Format("Id {0} does not exist in storage", counterId));

            return counter;
        }

        public IList<Counter> GetCounters(int accountId)
        {
            var counters = _counters.Where(x => x.OwnerAccountId == accountId).ToList();
            
            if (counters == null)
                throw new CounterRepositoryException(string.Format("No counters exist in storage for account id: {0}",accountId));
            return counters;
        }

        public void UpdateCounter (Counter counter)
        {
            var repositoryCounter = GetCounter(counter.Id.ToString());

            var counterIndex = _counters.IndexOf(repositoryCounter);
            _counters[counterIndex] = counter;
        }

        public void DeleteCounter(string counterid)
        {
            var counter = _counters.SingleOrDefault(x => x.Id == counterid);

            if (counter == null)
                return;

            _counters.Remove(counter);
        }

        public void SaveAccount(Account account)
        {
            if (GetAccount(account.Id) != null)
            {
                throw new CounterRepositoryException("Account already exists. Use Update Method");
            }
            
            if (account.Id == null || account.Id == 0)
                account.Id = GetNextAccountId();
            
            _accounts.Add(account);
        }

        public Account GetAccount(int accountId)
        {
            var account = _accounts.SingleOrDefault(x => x.Id == accountId);
            
                if (account == null)
                return null;

            account.Counters = _counters.Where(x => x.OwnerAccountId == accountId).ToList();

            return (account);                       
        }

        public Account GetAccount(string username)
        {
            var account = _accounts.SingleOrDefault(x => x.Name == username);

            if (account == null)
                return null;

            account.Counters = _counters.Where(x => x.OwnerAccountId == account.Id).ToList();

            return (account);         
        }       

        private Int32 GetNextAccountId ()
        {
            if (_accounts.Count == 0)
                return 1;

            var maxId = _accounts.Max(x => x.Id);

            return maxId+1;
        }
    }
}