﻿using System;

namespace CloudCounter.Domain
{
    public class CounterRepositoryException : Exception
    {
        public CounterRepositoryException(string message)
            : base(message)
        {

        }
    }
}