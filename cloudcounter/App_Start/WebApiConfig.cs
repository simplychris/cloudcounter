﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Tracing;
using CloudCounter.Domain.Interface;
using CloudCounter.Domain.Repository;
using CloudCounter.Domain.Services;
using log4net;
using log4net.Core;

namespace cloudcounter
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {


                

            config.Services.Replace(typeof(ITraceWriter), new Tracer());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{counterid}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "ActionRoute",
                routeTemplate: "api/{controller}/{action}/{counterid}",
                defaults: new {id = RouteParameter.Optional});
        }
    }
}
