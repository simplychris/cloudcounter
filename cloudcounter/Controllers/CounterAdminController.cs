﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CloudCounter.Domain.Interface;
using CloudCounter.Domain.IoC;
using CloudCounter.Domain.Model;
using StructureMap;

namespace cloudcounter.Controllers
{
    public class CounterAdminController : Controller
    {
        private ICloudCounterRepository _repository;

        [DefaultConstructor]
        public CounterAdminController()
        {
            _repository = IoCContainer.GetInstance<ICloudCounterRepository>();
        }



        public ActionResult List (int accountid=902)
        {

            ViewData.Model = _repository.GetCounters(accountid);
            return View ("~/Views/CounterAdmin/List.cshtml");
        }
    }
}
