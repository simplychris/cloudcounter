﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Tracing;
using StructureMap;
using log4net;

namespace cloudcounter.Controllers
{
    public class ValuesController : ApiController
    {
        private ILog _logger;
        
        public ValuesController ()
        {
            _logger = ObjectFactory.GetInstance<ILog>();
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            _logger.DebugFormat("GET: {0}", id);
            return "value";
        }

        // POST api/values
        public string Post([FromBody]string value)
        {
            Configuration.Services.GetTraceWriter().Info(Request, "ValuesController", "Post");            
            return string.Format("post Body: {0}", value);
        }

        // PUT api/values/5
        public String Put(int id, [FromBody]string value)
        {
            return string.Format("Put value: {0} Body {1}", id, value);
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}