﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CloudCounter.Domain.Infrastructure;
using CloudCounter.Domain.Interface;
using CloudCounter.Domain.Model;
using CloudCounter.Domain.Services;
using ActionResult = System.Web.Mvc.ActionResult;

namespace cloudcounter.Controllers
{
    public class SignUpController : Controller
    {
        private ICloudCounterRepository _repository;


        public SignUpController ()
        {
            
        }

        public SignUpController (ICloudCounterRepository repository)
        {
            _repository = repository;
        }
        //
        // GET: /SignUp/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /SignUp/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /SignUp/Create

        public String Create()
        {
            return ("In Create of SignUpController");
            //return View();
        }

        //
        // POST: /SignUp/Create

        [HttpPost]
        public ActionResult Create(SignUp signup)
        {
            try
            {                                

                if (signup.Password1 != signup.Password2)
                    ModelState.AddModelError("password1", "Passwords do not match");

                if (signup.UserName.Length > 0  && _repository.GetAccount(signup.UserName) != null)
                    ModelState.AddModelError("username", "Username already exists");

                if (ViewData.ModelState.IsValid)
                {
                    var newAccount = new Account() { Name = signup.UserName, Password = signup.Password1 };
                    _repository.SaveAccount(newAccount);
                    ControllerContext.HttpContext.Session[SessionNames.UserSession] =
                        new UserSession() {UserName = signup.UserName};
                    return RedirectToAction("Details", new RouteValueDictionary(new {controller = "Account", action = "Details", accountid=newAccount.Id}));                    
                }
            }
            catch
            {
                return View("Index", signup);
            }
            return View("Index",signup);

        }

        //
        // GET: /SignUp/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /SignUp/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here             
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /SignUp/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /SignUp/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
