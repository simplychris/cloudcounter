﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CloudCounter.Domain.Interface;
using CloudCounter.Domain.Model;
using StructureMap;

namespace cloudcounter.Controllers
{
    public class CounterController : ApiController
    {
        private ICloudCounterRepository _repository;

        [DefaultConstructor]
        public CounterController (ICloudCounterRepository repository)
        {
            _repository = repository;
        }
        // GET api/counter/1292-1023-1-199
        public String Get(string counterid)
        {
            var counter = _repository.GetCounter(counterid);

            return counter.Count.ToString();
        }

        // GET api/counter/5
        public string Get(int id)
        {
            return "Invalid Action";
        }

        // POST api/counter
        public void Post([FromBody]string value)
        {
        }

        [HttpGet]
        public String Increment (string counterid)
        {
            var counter = _repository.GetCounter(counterid);
            counter.Increment(1);
            _repository.UpdateCounter(counter);
            return counter.Count.ToString();
        }

        // PUT api/counter/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/counter/5
        public void Delete(int id)
        {
        }

        public void DeleteCounter (string counterid)
        {            
            _repository.DeleteCounter(counterid);
        }
    }
}
