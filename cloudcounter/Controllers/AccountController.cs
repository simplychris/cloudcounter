﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CloudCounter.Domain.Infrastructure;
using CloudCounter.Domain.Interface;
using CloudCounter.Domain.Model;
using ActionResult = System.Web.Mvc.ActionResult;

namespace cloudcounter.Controllers
{
    public class AccountController : Controller
    {
        private ICloudCounterRepository _repository;

        public AccountController(ICloudCounterRepository repository)
        {
            _repository = repository;

        }

        //
        // GET: /Account/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Account/Details/5

        //[LocalAuthorize]
        public ActionResult Details(int accountid = -1)
        {
            Account usersAccount; 
            try
            {
                var userSession = (UserSession)ControllerContext.HttpContext.Session[SessionNames.UserSession];
                var username = userSession.UserName;
                usersAccount = _repository.GetAccount(username);
                if (usersAccount.Id != accountid)
                {
                    return View("~/Views/Shared/SimpleMessage.cshtml",
                                new SimpleMessage() {Message = "Not Authorized To View This Page"});
                }
            }
            catch (Exception)
            {
                return View("~/Views/Shared/SimpleMessage.cshtml",         
                    new SimpleMessage() { Message = "Not Authorized To View This Page" });
            }
            var account = _repository.GetAccount(accountid);
            return View("~/Views/Account/Details.cshtml", account);
        }

        //
        // GET: /Account/Create

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult CreateCounter(int Id = -1)
        {
            var account = _repository.GetAccount(Id);

            var createViewModel = new CreateCounterViewModel() {OwnerAccount = account, NewCounter = new Counter()};
            createViewModel.NewCounter.OwnerAccountId = account.Id;
            return View(createViewModel);
        }

        
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn (SignUp credentials)
        {
            var account = _repository.GetAccount(credentials.UserName);

            if (account == null || credentials.Password1 != account.Password)
            {
                ModelState.AddModelError("invalid", "Username or Password is Invalid");
                return RedirectToAction("SignIn", "Account");
            }
            CreateAndSetUserSession(credentials);
            return RedirectToAction("Details", "Account", new {accountid = account.Id});
        }

        private void CreateAndSetUserSession(SignUp credentials)
        {
            ControllerContext.HttpContext.Session["UserSession"] = new UserSession() {UserName = credentials.UserName};
            ControllerContext.HttpContext.User = new GenericPrincipal(new GenericIdentity(credentials.UserName), null);
        }

        [HttpPost]
        public ActionResult CreateCounter (Counter newcounter, int accountid)
        {
            try
            {
                newcounter.OwnerAccountId = accountid;
                newcounter.LastUpdated = DateTime.UtcNow;
               _repository.SaveCounter(newcounter);
                    
                // TODO: Add insert logic here
               return RedirectToAction("Details", new { accountid = newcounter.OwnerAccountId });
            }
            catch
            {
                return View();
            }
        }

        //
        // POST: /Account/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Account/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Account/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        public ActionResult DeleteCounter (string counterid)
        {
            var count = _repository.GetCounter(counterid);
            var userSession = (UserSession) ControllerContext.HttpContext.Session[SessionNames.UserSession];

            bool authorizedToDelete = false;
            Counter counter = new Counter();            
            try
            {
                var account = _repository.GetAccount(userSession.UserName);
                counter = _repository.GetCounter(counterid);

                authorizedToDelete = (account.Id == counter.OwnerAccountId);
            }

            catch(Exception ex)
            {
                authorizedToDelete = false;                
            }
            
            if (!authorizedToDelete)
            {
                return View("~/Views/Shared/SimpleMessage.cshtml", new SimpleMessage() { Message = "Not Authorized To Delete This Counter" });
            }

            _repository.DeleteCounter(counterid);
            return RedirectToAction("Details", "Account", new {accountid = counter.OwnerAccountId});
        }


    }
}
