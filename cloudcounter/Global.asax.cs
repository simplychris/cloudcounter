﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CloudCounter.Domain.Interface;
using CloudCounter.Domain.IoC;
using CloudCounter.Domain.Repository;
using CloudCounter.Domain.Services;
using CloudCounter.Domain.Utility;
using StructureMap;
using log4net;

namespace cloudcounter
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //System.Diagnostics.Debugger.Break();                                   
            var container = IoCContainer.Container;                        
            var resolver = new StructureMapResolver(container);
            
            ControllerBuilder.Current.SetControllerFactory(new ControllerFactory());
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
            resolver.Build();
            IoCContainer.GetInstance<SeedDataHelper>().SeedCounterData01(5);
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}