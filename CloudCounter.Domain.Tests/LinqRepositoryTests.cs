﻿using System;
using CloudCounter.Domain.Model;
using CloudCounter.Domain.Repository;
using NUnit.Framework;

namespace CloudCounter.Domain.Tests
{
    [TestFixture]
    public class LinqRepositoryTests
    {
        private LinqMemoryCounterRepository repository;


        [SetUp]
        public void TestSetup ()
        {
            repository = new LinqMemoryCounterRepository();
        }


        [Test]
        public void should_save_counter ()
        {
            var counter = new Counter(1) {Id = Guid.NewGuid().ToString()};           

            Assert.DoesNotThrow(()=>repository.SaveCounter(counter));
           
        }

        [Test]
        public void should_throw_exception_on_duplicate_save_attempt ()
        {
            var counter = new Counter(1);

            repository.SaveCounter(counter);           
           Assert.Throws<CounterRepositoryException>(()=>repository.SaveCounter(counter)); 
        }

        [Test]
        public void should_get_saved_counter ()
        {
            var counter = new Counter(1);
            repository.SaveCounter(counter);
            var testCounter = repository.GetCounter(counter.Id.ToString());
            Assert.That(testCounter.Id, Is.EqualTo(counter.Id));
        }

        [Test]
        public void get_should_throw_exception_when_asking_for_a_counter_that_does_not_exist ()
        {
            var counter = new Counter(1);
            Assert.Throws<CounterRepositoryException>(()=> repository.GetCounter(counter.Id.ToString()));
        }

        [Test]
        public void should_update_counter ()
        {
            var counter = new Counter(1);
            counter.Increment(10);
            var count = counter.GetCount();

            repository.SaveCounter(counter);
            counter.Increment(30);
            repository.UpdateCounter(counter);


            var repositoryCounter = repository.GetCounter(counter.Id.ToString());

            Assert.That(repositoryCounter.GetCount(), Is.EqualTo(40));
        }

        [Test]
        public void update_should_throw_exception_when_updating_a_counter_that_does_not_exist ()
        {
            var counter = new Counter(1);

            Assert.Throws<CounterRepositoryException>(()=>repository.UpdateCounter(counter));
        }

    }
}