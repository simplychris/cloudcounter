﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CloudCounter.Domain.Model;
using NUnit.Framework;

namespace CloudCounter.Domain.Tests
{
    [TestFixture]
    public class CounterTests
    {
            
        [Test]
        public void new_counter_should_have_a_non_null_id ()
        {
            var counter = new Counter(1);
            Assert.That(counter.Id, Is.Not.Null);
        }

        [Test]
        public void new_counter_should_have_a_valid_non_default_id ()
        {
            var counter = new Counter(1);
            var defaultGuid = new Guid();

            Assert.That(counter.Id, Is.Not.EqualTo(defaultGuid));
        }

        [Test]
        public void new_counter_should_initialize_to_zero ()
        {
            var counter = new Counter(1);
            Assert.That(counter.GetCount(), Is.EqualTo(0));
        }

        [Test]
        public void counter_should_increment ()
        {
            var counter = new Counter(1);
            counter.Increment(10);
            counter.Increment(15);
            Assert.That(counter.GetCount(), Is.EqualTo(25));
        }
    }
}
