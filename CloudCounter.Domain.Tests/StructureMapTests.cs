﻿using System.Diagnostics;
using CloudCounter.Domain.Interface;
using CloudCounter.Domain.IoC;
using CloudCounter.Domain.Services;
using NUnit.Framework;
using StructureMap;
using log4net;

namespace CloudCounter.Domain.Tests
{
    [TestFixture]
    public class StructureMapTests
    {
        [TestFixtureSetUp]
        public void FixtureSetup ()
        {
            IoCContainer.Configure();
        }


        [Test]
        public void should_instantiate_logger ( )
        {
            var logFactory = IoCContainer.GetInstance<ILogFactory>();
            var logger = IoCContainer.GetInstance<ILog>();
            Assert.That(logger, Is.Not.EqualTo(null));
            Assert.That(logger.IsDebugEnabled, Is.EqualTo(true));
        }
    }
}