﻿using CloudCounter.Domain.Interface;
using CloudCounter.Domain.Model;
using CloudCounter.Domain.Repository;
using NUnit.Framework;

namespace CloudCounter.Domain.Tests
{
    [TestFixture]
    public class AccountTests
    {
        private ICloudCounterRepository repository;
        
        [SetUp]
        public void TestSetup ()
        {
            repository = new LinqMemoryCounterRepository();
        }

        [Test]
        public void should_save_account ()
        {
            var account1 = new Account() {Id = 1, Name = "Account 1"};
            var account2 = new Account() {Id = 2, Name = "Account 2"};
            
            repository.SaveAccount(account1);
            repository.SaveAccount(account2);

            var repositoryAccount = repository.GetAccount(2);
            Assert.That(repositoryAccount.Name, Is.EqualTo("Account 2"));
        }


         [Test]
         public void should_populate_counter_graph_when_getting_account ()
         {
             
             var counter1 = new Counter(1);
             counter1.Increment(5);

             var counter2 = new Counter(1);
             counter2.Increment(10);

             repository.SaveCounter(counter1);
             repository.SaveCounter(counter2);

             var account1 = new Account() {Id = 1, Name = "Account 1"};
             repository.SaveAccount(account1);
             var account = repository.GetAccount(1);

             Assert.That(account.Counters.Count, Is.EqualTo(2));
             Assert.That(account.Counters[0].GetCount(), Is.EqualTo(5));
             Assert.That(account.Counters[1].GetCount(), Is.EqualTo(10));
         }

        [Test]
        public void new_account_should_have_a_non_null_counter_list ()
        {
            var account = new Account();
            Assert.That(account.Counters, Is.Not.Null);
        }
    }
}